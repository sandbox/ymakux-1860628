<?php

function taxonomy_megamenu_admin($form, &$form_state, $megamenu = NULL, $action = NULL) {

  $form['#tree'] = TRUE;

  $form['id'] = array(
    '#type' => 'value',
    '#value' => isset($megamenu['id']) ? $megamenu['id'] : NULL,
  );
  
  $form['terms'] = array(
    '#type' => 'value',
    '#value' => !empty($megamenu['terms']) ? $megamenu['terms'] : array(),
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'), 
    '#default_value' => !empty($megamenu['name']) ? check_plain($megamenu['name']) : '',
    '#description' => t('A human name for this megamenu.'),
  );

  $form['vid'] = array(
    '#type' => 'select',
    '#required' => TRUE,
    '#title' => t('Select vocabulary'), 
    '#default_value' => isset($megamenu['vid']) ? $megamenu['vid'] : NULL, 
    '#options' => taxonomy_megamenu_vocabulary_options(),
    '#ajax' => array(
      'callback' => 'taxonomy_megamenu_admin_ajax_callback',
      'wrapper' => 'taxonomy-megamenu-admin-wrapper',
      'method' => 'replace',
    ),
    '#description' => t('Choose a vocabulary whose terms you want to use for this megamenu'),
  );
  
  $form['data'] = array(
   '#type' => 'container',
   '#attributes' => array('id' => 'taxonomy-megamenu-admin-wrapper'),
  );
  
  $form['data']['style'] = array(
    '#type' => 'select',
    '#title' => t('Style'), 
    '#default_value' => isset($megamenu['data']['style']) ? $megamenu['data']['style'] : NULL, 
    '#options' => taxonomy_megamenu_styles_options(),
    '#description' => t('A style for this megamenu. See module\'s "styles" folder'),
  );
  
  $form['data']['megamenu'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dropdown Megamenu'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $vid = !empty($form_state['values']['vid']) ? $form_state['values']['vid'] : NULL;
    
  if ($megamenu) {
    if (!empty($form_state['values']['vid'])) {
      $vid = $form_state['values']['vid'];
    }
    else {
      $vid = $megamenu['vid'];
    }
  }
    
  if ($vid) {
    $instances = field_info_instances('taxonomy_term');
    $vocab = taxonomy_vocabulary_load($vid);
    $machine_name = $vocab->machine_name;

    $options = array();
    if (!empty($instances[$machine_name])) {
      foreach ($instances[$machine_name] as $field) {
        if ($field['widget']['module'] == 'image') {
          $options[$field['field_name']] = t('@human_name (@system_name)', array(
            '@human_name' => $field['label'], '@system_name' => $field['field_name']));
        }
      }
    }
    
    if ($options) {
      $options[0] = t('- No icons -');
      $form['data']['megamenu']['image_field'] = array(
        '#type' => 'select',
        '#title' => t('Icon source'),
        '#default_value' => isset($megamenu['data']['megamenu']['image_field']) ? $megamenu['data']['megamenu']['image_field'] : 0,
        '#options' => $options,
        '#description' => t('Megamenu can have icons.
          You can set which field use as image source for the icons'),
      );
    
      $form['data']['megamenu']['image_style'] = array(
        '#type' => 'select',
        '#title' => t('Use image style'),
        '#default_value' => !empty($megamenu['data']['megamenu']['image_style']) ? $megamenu['data']['megamenu']['image_style'] : '',
        '#options' => image_style_options(),
        '#description' => t('Image style for icons'),
      );
    }
  }
  
  if (module_exists('search')) {
    $form['data']['megamenu']['search'] = array(
      '#type' => 'checkbox',
      '#title' => t('Attach search field'),
      '#description' => t('Attach to the megamenu search form provided by Search module'),
      '#default_value' => !empty($megamenu['data']['megamenu']['search']) ? $megamenu['data']['megamenu']['search'] : 0,
      '#description' => t('If selected, a search field from "Search" module will be appended to the megamenu.'),
    );
  }
  
  $form['data']['megamenu']['max_terms'] = array(
    '#type' => 'textfield',
    '#title' => t('Max top terms'),
    '#size'=> 3,
    '#default_value' => !empty($megamenu['data']['megamenu']['max_terms']) ? $megamenu['data']['megamenu']['max_terms'] : '',
    '#description' => t('How many top links do you want to display? Left blank to set no limit.'),
  );
  
  $form['data']['megamenu']['more_link'] = array(
    '#type' => 'checkbox',
    '#title' => t('"More" link'),
    '#default_value' => isset($megamenu['data']['megamenu']['more_link']) ? $megamenu['data']['megamenu']['more_link'] : 0,
    '#states' => array(
      'visible' => array(
       ':input[name="data[megamenu][max_terms]"]' => array('filled' => TRUE),
      ),
    ),
    '#description' => t('Prepend "More" link that will contain all remaining top links.'),
  );

  $form['data']['megamenu']['more_text'] = array(
    '#type' => 'textfield',
    '#title' => t('More link text'), 
    '#default_value' => !empty($megamenu['data']['megamenu']['more_text']) ? check_plain($megamenu['data']['megamenu']['more_text']) : t('More...'),
    '#states' => array(
      'visible' => array(
       ':input[name="data[megamenu][more_link]"]' => array('checked' => TRUE),
      ),
    ),
    '#description' => t('Custom text for "More" link'),
  );
    
  $form['data']['megamenu']['more_url'] = array(
    '#type' => 'textfield',
    '#title' => t('More link URL'), 
    '#default_value' => !empty($megamenu['data']['megamenu']['more_url']) ? check_plain($megamenu['data']['megamenu']['more_url']) : '',
    '#states' => array(
      'visible' => array(
       ':input[name="data[megamenu][more_link]"]' => array('checked' => TRUE),
      ),
    ),
    '#description' => t('Yu can define any custom address for "More" link'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  
  return $form;
}

function taxonomy_megamenu_admin_ajax_callback($form, &$form_state) {
  return $form['data'];
}

function taxonomy_megamenu_admin_validate($form, &$form_state) {
  
  $values = $form_state['values'];
  $url = $values['data']['megamenu']['more_url'];
  $terms = $values['data']['megamenu']['max_terms'];
  
  if (drupal_strlen($terms) && !is_numeric($values['data']['megamenu']['max_terms'])) {
    form_set_error('data][megamenu][max_terms', t('Wrong format.'));
  }

  if (drupal_strlen($url) != drupal_strlen(drupal_strip_dangerous_protocols($url))) {
    form_set_error('data][megamenu][more_url', t('Wrong URL format.'));
  }
  
  if (drupal_strlen($url) && (!valid_url($url, FALSE) || url_is_external($url))) {
    form_set_error('data][megamenu][more_url', t('Wrong URL format.'));
  }
  
  if ($values['data']['megamenu']['more_link'] && !drupal_strlen($values['data']['megamenu']['more_text'])) {
    form_set_error('data][megamenu][more_text', t('Please enter a text for "More" link.'));
  }

  form_set_value($form['terms'], serialize($values['terms']), $form_state);
  form_set_value($form['data'], serialize($values['data']), $form_state);
}

function taxonomy_megamenu_admin_submit($form, &$form_state) {
  
  form_state_values_clean($form_state);
  if (taxonomy_megamenu_megamenu_set($form_state['values'])) {
    drupal_set_message(t('The configuration options have been saved.'));
    $form_state['redirect'] = 'admin/structure/taxonomy_megamenu';
  }
}

function taxonomy_megamenu_delete($form, &$form_state, $megamenu) {

  $name = $megamenu['name'];
  
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $megamenu['id'],
  );
  
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $name,
  );
  
  return confirm_form($form,
    t('Are you sure you want to delete megamenu %name?', array('%name' => $name)),
    'admin/structure/taxonomy_megamenu',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

function taxonomy_megamenu_term_delete_confirm($form, &$form_state, $term_id, $megamenu_id) {

  $name = '';
  if (is_numeric($term_id)) {
    $term = taxonomy_term_load($term_id);
    $name = $term->name;
  }
  
  $form['term_id'] = array(
    '#type' => 'value',
    '#value' => $term_id,
  );
  
  $form['megamenu_id'] = array(
    '#type' => 'value',
    '#value' => $megamenu_id,
  );
  
  $form['term_name'] = array(
    '#type' => 'value',
    '#value' => $name,
  );
  
  return confirm_form($form,
    t('Are you sure you want to delete megamenu term %name?', array('%name' => $name)),
    $_GET['destination'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}


function taxonomy_megamenu_block_delete_confirm($form, &$form_state, $term_id, $megamenu_id, $block_id) {
  
  $form['term_id'] = array(
    '#type' => 'value',
    '#value' => $term_id,
  );
  
  $form['megamenu_id'] = array(
    '#type' => 'value',
    '#value' => $megamenu_id,
  );
  
  $form['block_id'] = array(
    '#type' => 'value',
    '#value' => $block_id,
  );
  
  return confirm_form($form,
    t('Are you sure you want to delete this block?'),
    $_GET['destination'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel'));
}

function taxonomy_megamenu_block_delete_confirm_submit($form, &$form_state) {
  
  $term_id = $form_state['values']['term_id'];
  $megamenu_id = $form_state['values']['megamenu_id'];
  $block_id = $form_state['values']['block_id'];
  
  if (taxonomy_megamenu_block_delete($term_id, $megamenu_id, $block_id)) {
    drupal_set_message(t('The configuration options have been saved.'));
    $form_state['redirect'] = "admin/structure/taxonomy_megamenu/$megamenu_id/term/$term_id/blocks";
  }
}

function taxonomy_megamenu_term_delete_confirm_submit($form, &$form_state) {

  $term_id = $form_state['values']['term_id'];
  $megamenu_id = $form_state['values']['megamenu_id'];
  
  if (taxonomy_megamenu_term_delete($term_id, $megamenu_id)) {
    drupal_set_message(t('The configuration options have been saved.'));
    $form_state['redirect'] = "admin/structure/taxonomy_megamenu/$megamenu_id/terms";
  }
}

function taxonomy_megamenu_delete_submit($form, &$form_state) {

  $name = $form_state['values']['name'];
  if (taxonomy_megamenu_megamenu_delete($form_state['values']['id'])) {
    drupal_set_message(t('Megamenu %name has been deleted', array('%name' => $name)));
    $form_state['redirect'] = 'admin/structure/taxonomy_megamenu';
  }
}

function taxonomy_megamenu_blocks_form($form, &$form_state, $megamenu, $term_id) {

  $form['#tree'] = TRUE;
  
  $megamenu_id = $megamenu['id'];

  $form['megamenu_id'] = array(
    '#type' => 'value',
    '#value' => $megamenu_id,
  );
    
  $form['term_id'] = array(
    '#type' => 'value',
    '#value' => $term_id,
  );
  
  drupal_set_title(t('Content of @name', array('@name' => $megamenu['terms'][$term_id]['name'])));
  
  if (!empty($megamenu['terms'][$term_id]['blocks'])) {
    
    $blocks = $megamenu['terms'][$term_id]['blocks'];
    uasort($blocks, 'drupal_sort_weight');
    
    foreach ($blocks as $key => $block) {

      $form['table'][$key]['name'] = array(
        '#markup' => check_plain($block['name']),
      );
      
      $form['table'][$key]['cols'] = array(
        '#type' => 'select',
        '#title' => t('Width (columns)'),
        '#title_display' => 'invisible',
        '#options' => taxonomy_megamenu_styles_options($megamenu['data']['style'], 'cols'),
        '#default_value' => !empty($block['cols']) ? $block['cols'] : 1,
      );
    
      $form['table'][$key]['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Block title'),
        '#title_display' => 'invisible',
        '#default_value' => !empty($block['title']) ? $block['title'] : '',
      );
      
      $form['table'][$key]['weight'] = array(
        '#type' => 'weight', 
        '#title' => '<span class="element-invisible">' . t('Weight') . '</span>',
        '#default_value' => $block['weight'],
        '#delta' => 10,
      );
      
      $operations = array();
      $status = $block['enabled'];
      $title = $status ? t('Disable') : t('Enable');
    
      $operations[] = array(
        'title' => $title,
        'href' => 'admin/structure/taxonomy_megamenu/actions',
        'query' => array(
          'action' => $status ? 'disable' : 'enable',
          'term_id' => $term_id,
          'megamenu_id' => $megamenu_id,
          'block_key' => $key,
          'token' => drupal_get_token(),
          'destination' => $_GET['q'],
        ),
      );

      $operations[] = array(
        'title' => t('Delete'),
        'href' => 'admin/structure/taxonomy_megamenu/actions',
        'query' => array(
          'action' => 'delete',
          'term_id' => $term_id,
          'megamenu_id' => $megamenu_id,
          'block_key' => $key,
          'token' => drupal_get_token(),
          'destination' => $_GET['q'],
        ),
      );
      
      if ($key == 'taxonomy_megamenu_children') {
        $operations[] = array(
          'title' => t('Children'),
          'href' => 'admin/structure/taxonomy_megamenu/' . $megamenu['id'] . '/term/' . $term_id . '/children',
        );
      }
      
      if (module_exists('ctools')) {
        $operations = theme('ctools_dropdown', array('title' => t('Select'), 'links' => $operations));
      }
      else {
        $operations = theme('links', array(
          'links' => $operations,
          'attributes' => array(
            'class' => array('links', 'inline'))));
      }
      
      $form['table'][$key]['operations'] = array('#markup' => $operations);
    }
  
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }
  return $form;
}

function taxonomy_megamenu_children_form($form, &$form_state, $megamenu, $term_id) {
  
  $form['#tree'] = TRUE;
  $vid = $megamenu['vid'];

  $form['megamenu_id'] = array(
    '#type' => 'value',
    '#value' => $megamenu['id'],
  );
    
  $form['term_id'] = array(
    '#type' => 'value',
    '#value' => $term_id,
  );
  
  if ($children = taxonomy_megamenu_flatten_children_options($vid, $term_id)) {
    
    foreach ($children as $term) {
      
      $form['table'][$term->tid]['name'] = array(
        '#markup' => t('@term_name (Term ID: @tid)', array('@term_name' => entity_label('taxonomy_term', $term), '@tid' => $term->tid)),
      );
      
      $form['table'][$term->tid]['megamenu'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable for dropdown megamenu'),
        '#title_display' => 'invisible',
        '#default_value' => !empty($megamenu['data']['megamenu']['disabled'][$term->tid]) ? 0 : 1,
      );
    }
  
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }
  return $form;
}

function taxonomy_megamenu_terms_form($form, &$form_state, $megamenu) {

  $form['#tree'] = TRUE;
  $rows = array();
  
  $form['id'] = array(
    '#type' => 'value',
    '#value' => isset($megamenu['id']) ? $megamenu['id'] : NULL,
  );
    
  $form['terms'] = array(
    '#type' => 'value',
    '#value' => !empty($megamenu['terms']) ? $megamenu['terms'] : array(),
  );

  if (!empty($megamenu['terms'])) {
    $terms = $megamenu['terms'];
    uasort($terms, 'drupal_sort_weight');

    foreach ($terms as $key => $term) {
      
      $term_name = $term['name'];
      
      if (!$term_name && is_numeric($key)) {
        $object = taxonomy_term_load($key);
        $term_name = entity_label('taxonomy_term', $object);
      }
      
      $form['table'][$key]['name'] = array(
        '#markup' => check_plain($term_name),
      );
    
      $blocks = array();
      if (!empty($term['blocks'])) {
        foreach ($term['blocks'] as $block) {
          $blocks[] = check_plain($block['name']);
        }
      }
    
      $form['table'][$key]['blocks'] = array(
        '#markup' => implode(',', $blocks),
      );
    
      $form['table'][$key]['align'] = array(
        '#markup' => $terms[$key]['align'],
      );
    
      $form['table'][$key]['cols'] = array(
        '#markup' => $terms[$key]['cols'],
      );
      
      $form['table'][$key]['state'] = array(
        '#markup' => $terms[$key]['enabled'] ? t('Enabled') : t('Disabled'),
      );
    
      $form['table'][$key]['weight'] = array(
        '#type' => 'weight', 
        '#title' => '<span class="element-invisible">' . t('Weight') . '</span>',
        '#default_value' => $terms[$key]['weight'],
        '#delta' => 10,
      );

      $operations = array();
      $status = $terms[$key]['enabled'];
      $title = $status ? t('Disable') : t('Enable');
    
      $operations[] = array(
        'title' => $title,
        'href' => "admin/structure/taxonomy_megamenu/actions",
        'query' => array(
          'action' => $status ? 'disable' : 'enable',
          'term_id' => $key,
          'megamenu_id' => $megamenu['id'],
          'token' => drupal_get_token(),
          'destination' => $_GET['q'],
        ),
      );

      $operations[] = array(
        'title' => t('Delete'),
        'href' => 'admin/structure/taxonomy_megamenu/actions',
        'query' => array(
          'action' => 'delete',
          'term_id' => $key,
          'megamenu_id' => $megamenu['id'],
          'token' => drupal_get_token(),
          'destination' => $_GET['q'],
        ),
      );
      
      $operations[] = array(
        'title' => t('Edit'),
        'href' => 'admin/structure/taxonomy_megamenu/' . $megamenu['id'] . '/term/' . $key . '/edit',
      );

      if (!empty($terms[$key]['blocks'])) {
        $operations[] = array(
          'title' => t('Content'),
          'href' => 'admin/structure/taxonomy_megamenu/' . $megamenu['id'] . '/term/' . $key . '/blocks',
        );
      }
      
      if (module_exists('ctools')) {
        $operations = theme('ctools_dropdown', array('title' => t('Select'), 'links' => $operations));
      }
      else {
        $operations = theme('links', array(
          'links' => $operations,
          'attributes' => array(
            'class' => array('links', 'inline'))));
      }
        
      $form['table'][$key]['operations'] = array('#markup' => $operations);
    }

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#submit' => array('taxonomy_megamenu_terms_submit'),
    );
  }
  return $form;
}

function taxonomy_megamenu_term_edit_form($form, &$form_state, $megamenu, $term_id, $action) {

  $form['#tree'] = TRUE;
  $top_terms = taxonomy_megamenu_top_terms($megamenu);
  $blocks = _taxonomy_megamenu_block_options();
  
  if ($action == 'add') {
    drupal_set_title(t('Add new item'));
  }

  $form['all_blocks'] = array(
    '#type' => 'value',
    '#value' => $blocks,
  );
    
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $megamenu['id'],
  );
    
  $form['term_id'] = array(
    '#type' => 'value',
    '#value' => $term_id,
  );
    
  $form['terms'] = array(
    '#type' => 'value',
    '#value' => !empty($megamenu['terms']) ? $megamenu['terms'] : array(),
  );

  $form['action'] = array(
    '#type' => 'value',
    '#value' => $action,
  );
    
  $edit = FALSE;
  if ($action == 'edit' && $term_id) {
    $edit = TRUE;
  }
    
  $settings = !empty($megamenu['terms'][$term_id]) ? $megamenu['terms'][$term_id] : array();
  $form['all_terms'] = array(
    '#type' => 'value',
    '#value' => $top_terms,
  );
  
  if (!empty($top_terms) && !empty($megamenu['terms'])) {
    $options = array();
    $megamenu_terms = array_keys($megamenu['terms']);
    foreach ($top_terms as $id => $top_term) {
      if (in_array($id, $megamenu_terms)) {
        unset($top_terms[$id]);
      }
    }
  }
  
  if (!$edit) {
    $form['data']['term'] = array(
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('Top term'),
      '#options' => $top_terms,
    );
   } 
    
  if ($edit) {
    $form['data']['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Title'), 
      '#default_value' => $settings['name'],
      '#description' => t('Label of the element. Term name by default'),
    );
  }
    
  $form['data']['align'] = array(
    '#type' => 'select',
    '#title' => t('Align'), 
    '#options' => array('right' => t('Right'), 'left' => t('Left')),
    '#default_value' => $edit ? $settings['align'] : 'left',
    '#description' => t('Dropdown container align, either left or right'),
  );
    
  $form['data']['cols'] = array(
    '#type' => 'select',
    '#title' => t('Columns'), 
    '#options' => taxonomy_megamenu_styles_options($megamenu['data']['style'], 'cols'),
    '#default_value' => $edit ? $settings['cols'] : NULL,
    '#description' => t('Width of dropdown container, in columns.
      The column widths should be described in the css file of your current style'),
  );
    
  $form['data']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#default_value' => $edit ? $settings['enabled'] : 1,
    '#description' => t('Enable this element to display.'),
  );
  
  $form['data']['weight'] = array(
    '#type' => 'weight', 
    '#title' => t('Weight'),
    '#delta' => 10,
    '#default_value' => $edit ? $settings['weight'] : 0,
    '#description' => t('Position of the element.'),
  );

  $form['data']['blocks'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#title' => t('Content'),
    '#options' => $blocks,
    '#default_value' => ($edit && !empty($settings['blocks'])) ?
      array_keys($settings['blocks']) : array('taxonomy_megamenu_children'),
    '#description' => t('Content of the element. Can be either children terms or regular Drupal\'s block.'),
  );
    
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function taxonomy_megamenu_term_edit_form_validate($form, &$form_state) {
  
  $action = $form_state['values']['action'];
  $term_id = $form_state['values']['term_id'];
  $existing_terms = $form_state['values']['terms'];
  
  $block_weight = 0;
  $block_enabled = 1;
  
  if ($blocks = $form_state['values']['data']['blocks']) {
    foreach ($blocks as $key => $block) {
      if ($action == 'add') {
        $form_state['values']['data']['blocks'][$key] = array(
          'name' => $form_state['values']['all_blocks'][$key],
          'weight' => $block_weight,
          'enabled' => $block_enabled,
          'title' => '',
          'cols' => 1,
        );
      }

      if ($action == 'edit') {
        if (isset($existing_terms[$term_id]['blocks'][$key]['weight'])) {
          $block_weight = $existing_terms[$term_id]['blocks'][$key]['weight'];
        }
        
        if (isset($existing_terms[$term_id]['blocks'][$key]['enabled'])) {
          $block_enabled = $existing_terms[$term_id]['blocks'][$key]['enabled'];
        }
        
        $form_state['values']['data']['blocks'][$key] = array(
          'name' => $form_state['values']['all_blocks'][$key],
          'weight' => $block_weight,
          'enabled' => $block_enabled,
          'title' => isset($existing_terms[$term_id]['blocks'][$key]['title']) ? $existing_terms[$term_id]['blocks'][$key]['title'] : '',
          'cols' => isset($existing_terms[$term_id]['blocks'][$key]['cols']) ? $existing_terms[$term_id]['blocks'][$key]['cols'] : 0,
        );
      }
    }
  }
  
  if ($action == 'add' && isset($form_state['values']['data']['term'])) {
    $new_term = $form_state['values']['data']['term'];
    if ($existing_terms) {
      if (in_array($new_term, array_keys($existing_terms))) {
        form_set_error('data[term', t('The term you selected already in use'));
        return;
      }
    }
      
    $form_state['values']['terms'] = array();
    $existing_terms[$new_term] = $form_state['values']['data'];
    $existing_terms[$new_term]['name'] = $form_state['values']['all_terms'][$new_term];
  }
  
  if ($action == 'edit') {
      $existing_terms[$term_id] = $form_state['values']['data'];
  }
  
  $modified_terms = $existing_terms;

  foreach ($form_state['values'] as $key => &$value) {
    if (!in_array($key, array('id', 'terms', 'action'))) {
      unset($form_state['values'][$key]);
    }
  }
  form_set_value($form['terms'], serialize($modified_terms), $form_state);
}

function taxonomy_megamenu_term_edit_form_submit($form, &$form_state) {

  $action = $form_state['values']['action'];
  unset($form_state['values']['action']);
  form_state_values_clean($form_state);
 
  if (taxonomy_megamenu_megamenu_set($form_state['values'])) {
   drupal_set_message(t('The configuration options have been saved.'));
   if ($action == 'edit') {
     
     $form_state['redirect'] = 'admin/structure/taxonomy_megamenu/' . $form_state['values']['id'] . '/terms';
   }
 }
 
}

function taxonomy_megamenu_children_form_submit($form, &$form_state) {

  $menu = array();
  $megamenu_id = $form_state['values']['megamenu_id'];
  $values = $form_state['values']['table'];
  $megamenu = taxonomy_megamenu_megamenu_get($megamenu_id);

  foreach ($values as $tid => $data) {
    if ($values[$tid]['megamenu'] == 0) {
      $menu[$tid] = 1;
    }
  }
  
  $megamenu['data']['megamenu']['disabled'] = $menu;

  if (taxonomy_megamenu_megamenu_set(array('id' => $megamenu_id, 'data' => serialize($megamenu['data'])))) {
    drupal_set_message(t('The configuration options have been saved.'));
  }
}

function taxonomy_megamenu_terms_submit($form, &$form_state) {
  
  $megamenu_id = $form_state['values']['id'];
  $values = $form_state['values']['table'];
  
  foreach ($values as $term_id => $value) {
    taxonomy_megamenu_term_set($term_id, $megamenu_id, array('weight' => $value['weight']));
  }
}

function taxonomy_megamenu_blocks_form_submit($form, &$form_state) {

  $megamenu_id = $form_state['values']['megamenu_id'];
  $term_id = $form_state['values']['term_id'];
  $values = $form_state['values']['table'];
  
  foreach ($values as $block_id => $block) {
    taxonomy_megamenu_block_set($term_id, $megamenu_id, $block_id, array(
      'weight' => $block['weight'],
      'cols' => $block['cols'],
      'title' => $block['title']));
  }
  drupal_set_message(t('The configuration options have been saved.'));
}
