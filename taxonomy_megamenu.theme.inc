<?php

/**
 * Theme function. Returns html of list of flatten children terms
 */
function theme_taxonomy_megamenu_flatten_children_list($variables) {

  $output = '';
  foreach ($variables['tree'] as $term) {
    if (!empty($variables['megamenu']['data']['megamenu']['disabled'][$term->tid])) {
      continue;
    }

    $title = entity_label('taxonomy_term', $term);
    $output .= '<li class="item">' . l($title, 'taxonomy/term/' . $term->tid, array(
      'attributes' => array(
        'title' => $term->description ? check_plain($term->description) : $title,
      ))) . '</li>';
  }
  return $output;
}

/**
 * Theme function. Returns html of list of megamenu
 */
function theme_taxonomy_megamenu_list($variables) {

  $output = '';
  $image_field = $variables['megamenu']['data']['megamenu']['image_field'];
  $style = $variables['megamenu']['data']['megamenu']['image_style'];

  $i = 0;
  foreach ($variables['tree'] as $term) {
    $url = 'taxonomy/term/' . $term->tid;
    $output .= '<div class="chunk-wrapper chunk-' . $i++ . '">';
    $image = '';

    if ($image_field) {
      $settings = array('label'=>'hidden');
      if ($style) {
        $settings['settings']['image_style'] = $style;
      }

      $term_object = taxonomy_term_load($term->tid);
      if ($image_view_field = field_view_field('taxonomy_term', $term_object, $image_field, $settings)) {
        $image = render($image_view_field);
      }

      if ($url && $image) {
        $image = l($image, $url, array('html' => TRUE));
      }

      $output .= '<div class="image">' . $image . '</div>';
    }

    $output .= '<ul class="chunk">';
    $title = entity_label('taxonomy_term', $term);

    $output .= '<li class="top-item item-' . $i++ . '">' . l($title, $url, array(
      'attributes' => array(
        'title' => $term->description ? check_plain($term->description) : $title,
        'class' => array(
          'top-item', 'item-' . $i++)))) . '</li>';
  

    if (!empty($term->children)) {
      $children = taxonomy_megamenu_build_taxonomy_tree($variables['megamenu']['vid'], $term->tid);
      $tree = taxonomy_megamenu_flatten_children($children);

      $output .= theme('taxonomy_megamenu_flatten_children_list', array(
        'tree' => $tree,
        'megamenu' => $variables['megamenu']));
    }

    $output .= '</ul>';
    $output .= '</div>';
  }
  return $output;
}

/**
 * Theme function. Returns html of list of nested blocks
 */
function theme_taxonomy_megamenu_children_block($variables) {

  $attributes = array(
    'class' => array(
      'children-terms',
      'vid-' . $variables['taxonomy']['vid'],
      'parent-' . $variables['taxonomy']['parent'],
      'block',
    ),
  );

  $output = '<div' . drupal_attributes($attributes) .'>';
    if ($variables['title']) {
      $output .= '<h2' . drupal_attributes(array('class' => array('block-title'))) .'>';
      $output .= $variables['title'];
      $output .= '</h2>';
    }

  $output .= '<div' . drupal_attributes(array('class' => array('content', 'clearfix'))) .'>';
  $output .= $variables['content'];
  $output .= '</div>';
  $output .= '</div>';
  
  return $output;
}

/**
 * Theme function. Returns html of whole megamenu
 */
function theme_taxonomy_megamenu($variables) {

  $output = '';
  $megamenu = $variables['megamenu'];
  $terms = $variables['megamenu']['terms'];
  $vid = $variables['megamenu']['vid'];

  taxonomy_megamenu_prepare_top_links($terms, $megamenu);

  if (!$terms) {
    return;
  }
  
  uasort($terms, 'drupal_sort_weight');
  
  $more_text = $variables['megamenu']['data']['megamenu']['more_text'];
  $more_url = !empty($variables['megamenu']['data']['megamenu']['more_url']) ? $variables['megamenu']['data']['megamenu']['more_url'] : '';
    
  taxonomy_megamenu_style_add($megamenu);
  taxonomy_megamenu_js_search_form();
  
  $search_field_enabled = FALSE;
  $search_field = 'no-serch-field';
  if ($megamenu['data']['megamenu']['search'] && user_access('search content')) {
    $search_field_enabled = TRUE;
    $search_field = 'has-serch-field';
  }
    
  $output = '<div id="taxonomy-megamenu-wrap" class="' . $search_field . '">';

  if ($search_field_enabled) {
    $output .= '<div id="taxonomy-megamenu-search">';
    $form = drupal_get_form('search_block_form');
    $output .= drupal_render($form);
    $output .= '</div>';
  }

  $output .= '<ul id="taxonomy-megamenu">';
  foreach ($terms as $id => $term) {
    if (is_numeric($id)) {
      if (!$term['name']) {
        $object = taxonomy_term_load($id);
        $term_name = entity_label('taxonomy_term', $object);
      }
      else {
        $t = get_t();
        $term_name = $t($term['name']);
      } 

      $link = l($term_name, 'taxonomy/term/' . $id, array('attributes' => array('class' => 'drop')));
    }
    else {
      if ($more_url) {
         $link = l(t($more_text), $more_url, array('fragment' => ' '));
      }
      else {
         $link = l(t($more_text), '', array('fragment' => ' '));
      }
    }

    $parent_class = !empty($term['blocks']) ? 'children' : 'no-children';

    $output .= '<li class="' . $parent_class . '">' . $link;

    if (!empty($term['blocks'])) {
      $class = array();
      $class[] = 'dropdown-container';
      $class[] = ($term['cols'] == 1) ? 'dropdown_1column' : 'dropdown_' . $term['cols'] . 'columns';
      $class[] = !empty($term['align']) ? 'align_' . $term['align'] : 'align_right';

      $output .= '<div' . drupal_attributes(array('class' => $class)) .'>';

      uasort($term['blocks'], 'drupal_sort_weight');

      foreach ($term['blocks'] as $block_key => $data) {
      
        $title = ($data['title'] && $data['title'] != '<none>') ? t($data['title']) : '';
        if ($block_key == 'taxonomy_megamenu_children') {
          $image_field = $variables['megamenu']['data']['megamenu']['image_field'];
          $image_style = $variables['megamenu']['data']['megamenu']['image_style']; 

          if ($id == 'more_link') {
            if ($tree = taxonomy_megamenu_build_taxonomy_tree($vid, 0, NULL)) {
              foreach ($tree as $key => $value) {
                if (in_array($value->tid, array_keys($terms))) {
                  unset($tree[$key]);
                }
              }
            }
          }
          else {
            $tree = taxonomy_megamenu_build_taxonomy_tree($vid, $id, NULL);
          }

          $content = theme('taxonomy_megamenu_list', array(
            'tree' => $tree,
            'megamenu' => $variables['megamenu'],
          ));

          $content = theme('taxonomy_megamenu_children_block', array(
            'content' => $content,
            'taxonomy' => array('vid' => $vid, 'parent' => $id),
            'title' => $title));
        }
        else {
          $explode = explode('|', $block_key);
          $block = block_load($explode[0], $explode[1]);

          if ($block->module == 'views') {
            $explode = explode('-', $block->delta);
            $view = views_embed_view($explode[0], $explode[1], $id);

            $view = theme('taxonomy_megamenu_view_block', array(
              'view' => $view,
              'name' => $explode[0],
              'display' => $explode[1],
              'title' => $title,
            ));

           unset($term['blocks'][$block_key]);
          }

          $renderable_block =  _block_get_renderable_array(_block_render_blocks(array($block)));
          $block_key = str_replace('|', '_', $block_key);

          if ($title) {
            $renderable_block[$block_key]['#block']->subject = $title;
          }
          else {
            $renderable_block[$block_key]['#block']->subject = '';
          }

          if ($block->module == 'views') {
            $content = $view;
          }
          else {
            $content = drupal_render($renderable_block);
          }
        }
        $output .= '<div class="col_' . $data['cols'] . '">' . $content . '</div>';
      }
      $output .= '</div>';
    }
    $output .= '</li>';
  }

  $output .= '</ul>';
  $output .= '</div>';
  return $output;
}

/**
 * Theme function. Returns html of item content (block)
 */
function theme_taxonomy_megamenu_view_block ($variables) {

  $attributes = array('class' => array(
    str_replace('_', '-', $variables['name'] . '-' . $variables['display']),
    'block',
  ));

  $output = '<div' . drupal_attributes($attributes) .'>';

  if ($variables['title']) {
    $output .= '<h2' . drupal_attributes(array('class' => array('block-title'))) .'>';
    $output .= $variables['title'];
    $output .= '</h2>';
  }

  $output .= '<div' . drupal_attributes(array('class' => array('content', 'clearfix'))) .'>';
  $output .= $variables['view'];
  $output .= '</div>';
  $output .= '</div>';
  return $output;
}

/**
 * Theme function. Returns themed output for form items's content
 */
function theme_taxonomy_megamenu_blocks_form($variables) {

  $form = $variables['form'];
  $rows = array();

  if (!empty($form['table'])) {
    foreach (element_children($form['table'], TRUE) as $term) {
      $form['table'][$term]['weight']['#attributes']['class'] = array('taxonomy-megamenu-block-weight');
      $rows[] = array(
        'data' => array(
          drupal_render($form['table'][$term]['name']),
          drupal_render($form['table'][$term]['cols']),
          drupal_render($form['table'][$term]['title']),
          drupal_render($form['table'][$term]['weight']),
          drupal_render($form['table'][$term]['operations']),
        ),
        'class' => array('draggable'),
      );
    }
  }

  $header = array(t('Block'), t('Width (columns)'), t('Header'), t('Weight'), t('Operations'));
  $table_id = 'taxonomy-megamenu-blocks';

  $output = theme('table', array(
    'header' => $header,
    'rows'=> $rows,
    'empty' => t('This megamenu term has no blocks yet.'),
    'attributes' => array('id' => $table_id),
  ));

  drupal_add_tabledrag($table_id, 'order', 'sibling', 'taxonomy-megamenu-block-weight');
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Theme function. Returns themed output for form of megamenu elements (terms)
 */
function theme_taxonomy_megamenu_terms_form($variables) {

  $form = $variables['form'];
  $rows = array();

  if (!empty($form['table'])) {
    foreach (element_children($form['table'], TRUE) as $term) {
      $form['table'][$term]['weight']['#attributes']['class'] = array('taxonomy-megamenu-term-weight');
      $rows[] = array(
        'data' => array(
          drupal_render($form['table'][$term]['name']),
          drupal_render($form['table'][$term]['blocks']),
          drupal_render($form['table'][$term]['align']),
          drupal_render($form['table'][$term]['cols']),
          drupal_render($form['table'][$term]['state']),
          drupal_render($form['table'][$term]['weight']),
          drupal_render($form['table'][$term]['operations']),
        ),
        'class' => array('draggable'),
      );
    }
  }

  $header = array(
    t('Term'),
    t('Content'),
    t('Align'),
    t('Width (columns)'),
    t('State'),
    t('Weight'),
    t('Operations'),
  );
  
  $table_id = 'taxonomy-megamenu-terms';
  $output = theme('table', array(
    'header' => $header,
    'rows'=> $rows,
    'empty' => t('This megamenu has no items yet.'),
    'attributes' => array('id' => $table_id),
  ));

  drupal_add_tabledrag($table_id, 'order', 'sibling', 'taxonomy-megamenu-term-weight');
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Theme function. Returns themed output for form of nested terms
 */
function theme_taxonomy_megamenu_children_form($variables) {

  $form = $variables['form'];
  $rows = array();

  if (!empty($form['table'])) {
    foreach (element_children($form['table'], TRUE) as $term) {
      $rows[] = array(
        'data' => array(
          drupal_render($form['table'][$term]['name']),
          drupal_render($form['table'][$term]['megamenu']),
        ),
      );
    }
  }

  $header = array(t('Term'), t('Enabled for megamenu'));
  $output = theme('table', array(
    'header' => $header,
    'rows'=> $rows,
    'empty' => t('This term has no children.'),
  ));

  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Theme function. Returns html of table with all available megamenus
 */
function theme_taxonomy_megamenu_overview($variables) {
  $rows = array();
  $vocabularies = taxonomy_megamenu_vocabulary_options();
  if ($megamenus = taxonomy_megamenu_megamenu_get()) {

    foreach ($megamenus as $id => $megamenu) {
      $operations = array();
      if (user_access('administer taxonomy_megamenu')) {
        $operations[] = array(
          'title' => t('Edit'),
          'href' => "admin/structure/taxonomy_megamenu/$id/edit",
        );
        
        $operations[] = array(
          'title' => t('Delete'),
          'href' => "admin/structure/taxonomy_megamenu/$id/delete",
        );
      }
      
      if (user_access('administer taxonomy_megamenu terms') || user_access('administer taxonomy_megamenu')) {
        $operations[] = array(
          'title' => t('Edit top links'),
          'href' => "admin/structure/taxonomy_megamenu/$id/terms",
        );
      }
      
      if (module_exists('ctools')) {
        $operations = theme('ctools_dropdown', array('title' => t('Select'), 'links' => $operations));
      }
      else {
        $operations = theme('links', array(
          'links' => $operations,
          'attributes' => array(
            'class' => array('links', 'inline'))));
      }
      
      $max = $megamenu['data']['megamenu']['max_terms'] ? check_plain($megamenu['data']['megamenu']['max_terms']) : t('Unlimited');
      
      $rows[] = array(
        'data' => array(
          check_plain($megamenu['name']),
          check_plain($vocabularies[$megamenu['vid']]),
          count($megamenu['terms']),
          $max,
          $operations,
        ),
      );
    }
  }
  
  $header = array(
    array('data' => t('Name')),
    array('data' => t('Vocabulary')),
    array('data' => t('Terms')),
    array('data' => t('Max top links')),
    array('data' => t('Operations')),
  );
  return theme('table', array('header' => $header, 'rows'=>$rows, 'empty' => t('No taxonomy megamenus yet.')));
}
