The Taxonomy Megamenu module allows to display taxonomy tree from specific vocabulary
as a dropdown CSS Megamenu.

See http://net.tutsplus.com/tutorials/html-css-techniques/how-to-build-a-kick-butt-css3-mega-drop-down-menu

Installation
============

It's trivial. Download, unpack to "sites/all/modules" then enable on "admin/modules"

Usage
=====

You should have a vocabulary with terms in it.

 - Go to "admin/structure/taxonomy_megamenu"
 - Click "Add" to add a new taxonomy megamenu
 - Form fields are self-explanatory, you have to fill at least all required fields
 - On "admin/structure/taxonomy_megamenu" click "Select" -> "Edit top links"
 - Click "Add new item" to add a new top link (term). The top link can have two types of content -
   either blocks or its own nested taxonomy terms (if any). Choose what you want. Save it. Repeat.
 - Go to "admin/structure/block", find a block named "Taxonomy Megamenu <number>". Drop it into a region.
 Save blocks. See your cool megamenu.
 
Author
======
Iurii Makukh, Ukraine
ymakux@gmail.com